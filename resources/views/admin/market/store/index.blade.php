@extends('admin.layouts.master')
@section('head-tag')
    <title>انبار</title>
@endsection
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item font-size-12"> <a href="#">خانه</a></li>
            <li class="breadcrumb-item font-size-12"> <a href="#">بخش فروش</a></li>
            <li class="breadcrumb-item font-size-12 active" aria-current="page"> انبار</li>
        </ol>
    </nav>
    <section class="row">
        <section class="col-12">
            <section class="main-body-container">
                {{-- header --}}
                <section class="main-body-container-header">
                    <h6>انبار</h6>
                </section>
                {{-- button and search inout --}}
                <section class="pb-2 mt-4 mb-3 d-flex justify-content-between align-items-center border-bottom">
                    <a href="#" class="btn btn-info btn-sm disabled">ایجاد انبار جدید</a>
                    <div class="max-width-16-rem">
                        <input class="form-control form-control-sm form-text" type="text" name="" id="" placeholder="جستجو">
                    </div>
                </section>
                <section class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>نام کالا</th>
                                <th>تصویر کالا</th>
                                <th>موجودی</th>
                                <th>ورودی انبار</th>
                                <th>خروجی انبار</th>
                                <th class="text-center max-width-16-rem"><i class="fa fa-cogs"></i> تنظیمات</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>1</th>
                                <td>نمایشگر</td>
                                <td><img src="{{ asset('admin-assets/images/avatar-2.jpg') }}" alt="monitor"
                                    class="max-height-2rem"></td>
                                <td>16</td>
                                <td>38</td>
                                <td>22</td>
                                <td class="text-left width-22-rem">
                                    <a href="{{ route('admin.market.store.add-to-store') }}"
                                        class="btn btn-primary btn-sm"><i class="pl-1 fa fa-edit"></i> افزایش
                                        موجودی</a>
                                    <a href="#" class="btn btn-warning btn-sm"><i class="pl-1 fa fa-edit"></i> اصلاح
                                        موجودی</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </section>
        </section>
    </section>
@endsection
