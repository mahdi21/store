<header class="header">

    <section class="sidebar-header bg-gray">
        <section class="d-flex justify-content-between flex-md-row-reverse px-2">
            <span id="sidebar-toggle-show" class="d-inline d-md-none pointer"><i
                    class="fas fa-toggle-off"></i></span>
            <span id="sidebar-toggle-hide" class="d-none d-md-inline pointer"><i
                    class="fas fa-toggle-on"></i></span>
            <span><img class="logo" src="{{ asset('admin-assets/images/logo.png') }}" alt=""></span>
            <span class="d-md-none pointer" id="menu-toggle"><i class="fas fa-ellipsis-h"></i></span>
        </section>
    </section>


    <section id="body-header" class="body-header">
        <section class="d-flex justify-content-between">
            <!-- right -->
            <section>
                <!-- search -->
                <span class="mr-5">
                    <span id="search-area" class="search-area d-none">
                        <i id="search-area-hide" class="fas fa-times pointer"></i>
                        <input id="search-input" type="text" class="search-input" name="" id="">
                        <i class="fas fa-search pointer"></i>
                    </span>
                    <i id="search-toggle" class="fas fa-search p-1 d-none d-md-inline pointer"></i>
                </span>
                <!-- full screen -->
                <span id="full-screen" class="pointer d-none d-md-inline mr-5">
                    <i id="screen-compress" class="fas fa-compress d-none"></i>
                    <i id="screen-expand" class="fas fa-expand"></i>
                </span>
            </section>

            <!-- left -->
            <section>
                <!-- notification -->
                <span class="ml-2 ml-md-4 position-relative">
                    <!-- icon -->
                    <span id="header-notification-toggle" class="pointer">
                        <i class="far fa-bell ml-1"></i><sup class="badge badge-danger">4</sup>
                    </span>
                    <!-- notification box -->
                    <section id="header-notification" class="header-notification rounded">
                        <!-- header -->
                        <section class="d-flex justify-content-between">
                            <span class="px-2">نوتیفیکیشن ها</span>
                            <span class="px-2">
                                <span class="badge badge-danger">جدید</span>
                            </span>
                        </section>
                        <!-- notification list -->
                        <ul class="list-group rounded px-0">
                            <li class="list-group-item list-group-item-action">
                                <section class="media">
                                    <img class="notification-img" src="{{ asset('admin-assets/images/avatar-2.jpg') }}"
                                        alt="avatar">
                                    <section class="media-body pr-2">
                                        <h5 class="notification-user">مهدی آقامحمدی</h5>
                                        <p class="notification-text">این یک متن تستی است.</p>
                                        <p class="notification-time">30 دقیقه پیش</p>
                                    </section>
                                </section>
                            </li>
                            <li class="list-group-item list-group-item-action">
                                <section class="media">
                                    <img class="notification-img" src="{{ asset('admin-assets/images/avatar-2.jpg') }}"
                                        alt="avatar">
                                    <section class="media-body pr-2">
                                        <h5 class="notification-user">مهدی آقامحمدی</h5>
                                        <p class="notification-text">این یک متن تستی است.</p>
                                        <p class="notification-time">30 دقیقه پیش</p>
                                    </section>
                                </section>
                            </li>
                            <li class="list-group-item list-group-item-action">
                                <section class="media">
                                    <img class="notification-img" src="{{ asset('admin-assets/images/avatar-2.jpg') }}"
                                        alt="avatar">
                                    <section class="media-body pr-2">
                                        <h5 class="notification-user">مهدی آقامحمدی</h5>
                                        <p class="notification-text">این یک متن تستی است.</p>
                                        <p class="notification-time">30 دقیقه پیش</p>
                                    </section>
                                </section>
                            </li>
                        </ul>
                    </section>
                </span>
                <!-- comments -->
                <span class="ml-2 ml-md-4 position-relative">
                    <span id="header-comment-toggle" class="pointer">
                        <i class="far fa-comment-alt ml-1"></i><sup class="badge badge-danger">3</sup>
                    </span>


                    <!-- box comment -->
                    <section id="header-comment" class="header-comment">
                        <!-- search -->
                        <section class="border-bottom px-4">
                            <input type="text" class="form-control form-control-sm my-4" placeholder="جستجو">
                        </section>
                        <!-- comment list -->
                        <section class="header-comment-wrapper">
                            <ul class="list-group rounded px-0">
                                <li class="list-group-item list-group-item-action">
                                    <section class="media">
                                        <img src="{{ asset('admin-assets/images/avatar-2.jpg') }}" alt="avatar"
                                            class="notification-img">
                                        <section class="media-body pr-1">
                                            <section class="d-flex justify-content-between">
                                                <h5 class="comment-user">مهدی آقامحمدی</h5>
                                                <span class="fas fa-circle text-success comment-user-status"></span>
                                            </section>
                                        </section>
                                    </section>
                                </li>
                            </ul>
                        </section>
                    </section>
                </span>
                <!-- avatar -->
                <span class="ml-3 ml-md-5 position-relative">
                    <span id="header-profile-toggle" class="pointer">
                        <img src="{{ asset('admin-assets/images/avatar-2.jpg') }}" alt="avatar" class="header-avatar">
                        <span class="header-username">مهدی آقامحمدی</span>
                        <i class="fas fa-angle-down"></i>
                    </span>
                    <section id="header-profile" class="header-profile rounded">
                        <section class="list-group rounded">
                            <a href="#" class="list-group-item list-group-item-action header-profile-link">
                                <i class="fas fa-cog"></i>تنظیمات
                            </a>
                            <a href="#" class="list-group-item list-group-item-action header-profile-link">
                                <i class="fas fa-user"></i>کاربر
                            </a>
                            <a href="#" class="list-group-item list-group-item-action header-profile-link">
                                <i class="far fa-envelope"></i>پیام ها
                            </a>
                            <a href="#" class="list-group-item list-group-item-action header-profile-link">
                                <i class="fas fa-lock"></i>قفل صفحه
                            </a>
                            <a href="#" class="list-group-item list-group-item-action header-profile-link">
                                <i class="fas fa-sign-out-alt"></i>خروج
                            </a>
                        </section>
                    </section>
                </span>

            </section>
        </section>
    </section>

</header>